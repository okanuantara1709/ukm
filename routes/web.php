<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/','HomeController@index')->name('home.index');
// Route::get('/login','Auth\\LoginController@showLoginForm')->name('home.auth.login');
// Route::post('/login','Auth\\LoginController@login')->name('home.auth.login');
// Route::get('/logout','Auth\\LoginController@logout')->name('home.auth.logout');

// Route::get('/contact','HomeController@contact')->name('home.contact');
// Route::get('/akademik','HomeController@akademik')->name('home.akademik');

// Route::get('/{id}/show','HomeController@show')->name('home.show');
// Route::post('/store','HomeController@store')->name('home.store');

Route::group(['namespace' => 'Admin','prefix' => 'admin'],function(){
    //auth login
    Route::get('/','Auth\\LoginController@showLoginForm')->name('admin.auth');
    Route::get('/login','Auth\\LoginController@showLoginForm')->name('admin.auth.login');
    Route::post('/login','Auth\\LoginController@login')->name('admin.auth.login');
    Route::get('/logout','Auth\\LoginController@logout')->name('admin.auth.logout');

    Route::middleware('auth:admin')->group(function(){
        Route::get('/dashboard','DashboardController@index')->name('dashboard.index');    
    
        // Route::get('//pegawai','PegawaiController@index')->name('admin_pegawai_index');
        Route::get('/user/profile','UserController@profile')->name('user.profile');
        Route::resources([
            '/user' => 'UserController'
        ]);

       
        Route::get('/master_kelas/{id}/create_jadwal','MasterKelasController@createJadwal')->name('master_kelas.createJadwal');
        Route::get('/master_kelas/{id}/edit_jadwal','MasterKelasController@editJadwal')->name('master_kelas.editJadwal');
        Route::get('/master_kelas/{id}/delete_jadwal','MasterKelasController@deleteJadwal')->name('master_kelas.deleteJadwal');
        Route::put('/master_kelas/{id}/update_jadwal','MasterKelasController@updateJadwal')->name('master_kelas.updateJadwal');
        Route::post('/master_kelas/store_jadwal','MasterKelasController@storeJadwal')->name('master_kelas.storeJadwal');

        Route::get('/master_kelas/{id}/create_murid','MasterKelasController@createMurid')->name('master_kelas.createMurid');
        Route::get('/master_kelas/{id}/edit_murid','MasterKelasController@editMurid')->name('master_kelas.editMurid');
        Route::get('/master_kelas/{id}/delete_murid','MasterKelasController@deleteMurid')->name('master_kelas.deleteMurid');
        Route::put('/master_kelas/{id}/update_murid','MasterKelasController@updateMurid')->name('master_kelas.updateMurid');
        Route::post('/master_kelas/store_murid','MasterKelasController@storeMurid')->name('master_kelas.storeMurid');

        
        
        
       

        Route::get('/proposal/{id}/print','ProposalController@print')->name('proposal.print');
        Route::resources([
            '/proposal' => 'ProposalController'
        ]);
        
        Route::get('/jadwal/print','JadwalController@print')->name('jadwal.print');
        Route::resources([
            '/jadwal' => 'JadwalController'
        ]);

        Route::resources([
            '/jadwal_radio' => 'JadwalRadioController'
        ]);
        Route::get('/jadwal_radio/{id}/createUser','JadwalRadioController@createUser')->name('jadwal_radio.createUser');
        Route::get('/jadwal_radio/{id}/createKehadiran','JadwalRadioController@createKehadiran')->name('jadwal_radio.createKehadiran');
        Route::get('/jadwal_radio/{id}/editKehadiran','JadwalRadioController@editKehadiran')->name('jadwal_radio.editKehadiran');
        Route::get('/jadwal_radio/{id}/deleteUser','JadwalRadioController@deleteUser')->name('jadwal_radio.deleteUser');
        Route::get('/jadwal_radio/{id}/print','JadwalRadioController@print')->name('jadwal_radio.print');
        Route::post('/jadwal_radio/{id}/storeUser','JadwalRadioController@storeUser')->name('jadwal_radio.storeUser');
        Route::post('/jadwal_radio/{id}/storeKehadiran','JadwalRadioController@storeKehadiran')->name('jadwal_radio.storeKehadiran');
        Route::put('/jadwal_radio/{id}/updateKehadiran','JadwalRadioController@updateKehadiran')->name('jadwal_radio.updateKehadiran');
        Route::get('/jadwal_radio/{id}/showKehadiran','JadwalRadioController@showKehadiran')->name('jadwal_radio.showKehadiran');

        Route::resources([
            '/bidang' => 'BidangController'
        ]);


        Route::resources([
            '/iuran' => 'IuranController'
        ]);
        Route::get('/iuran/{id}/delete','IuranController@delete')->name('iuran.delete');

        Route::get('/kehadiran/{id}/index','KehadiranController@index')->name('kehadiran.index');
        Route::get('/kehadiran/{id}/create','KehadiranController@create')->name('kehadiran.create');
        Route::get('/kehadiran/{id}/edit','KehadiranController@edit')->name('kehadiran.edit');
        Route::post('/kehadiran/store','KehadiranController@store')->name('kehadiran.store');
        Route::put('/kehadiran/{id}/update','KehadiranController@update')->name('kehadiran.update');
        Route::get('/kehadiran/{id}/show','KehadiranController@show')->name('kehadiran.show');

        
       

        Route::resources([
            '/properti' => 'PropertiController'
        ]);
        Route::get('/properti/{id}/create_foto','PropertiController@createFoto')->name('properti.createFoto');
        Route::post('/properti/storeFoto','PropertiController@storeFoto')->name('properti.storeFoto');
        Route::get('/properti/{id}/deleteFoto','PropertiController@deleteFoto')->name('properti.deleteFoto');
        
    });
});
