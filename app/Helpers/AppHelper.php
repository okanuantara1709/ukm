<?php
namespace App\Helpers;
use Carbon\Carbon;
use Auth;

class AppHelper{
	/**
	*@example : GlobalHelper::selected($var1,$var2)
	*@retrun : boolean
	*/
    public static function selected($var1,$var2) {
        if($var1 == $var2){
        	return 'selected';
        }
    }


    /**
	*@example : GlobalHelper::selected($sting,$var2)
	*@retrun : boolean
	*@param 1 : string explode to array
	*@param 2 : string
	*/
    public static function selected_array($var1,$var2,$object=false) {
        // $var1 = (array) $var1;
        if(is_array($var1) || is_object($var1)){

            foreach ($var1 as $key => $value) {
                if($object == false){
                    if($value == $var2){
                        return 'selected';
                    }
                }else{
                    if($value->$object == $var2){
                        return 'selected';
                    }
                }
            }
        }
    }

    // untuk membuat nilai terbilang
    public static function terbilang($x, $style=3) {
        function kekata($x) {
            $x = abs($x);
            $angka = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
            $temp = "";
            if ($x <12) {
                $temp = " ". $angka[$x];
            } else if ($x <20) {
                $temp = kekata($x - 10). " belas";
            } else if ($x <100) {
                $temp = kekata($x/10)." puluh". kekata($x % 10);
            } else if ($x <200) {
                $temp = " seratus" . kekata($x - 100);
            } else if ($x <1000) {
                $temp = kekata($x/100) . " ratus" . kekata($x % 100);
            } else if ($x <2000) {
                $temp = " seribu" . kekata($x - 1000);
            } else if ($x <1000000) {
                $temp = kekata($x/1000) . " ribu" . kekata($x % 1000);
            } else if ($x <1000000000) {
                $temp = kekata($x/1000000) . " juta" . kekata($x % 1000000);
            } else if ($x <1000000000000) {
                $temp = kekata($x/1000000000) . " milyar" . kekata(fmod($x,1000000000));
            } else if ($x <1000000000000000) {
                $temp = kekata($x/1000000000000) . " trilyun" . kekata(fmod($x,1000000000000));
            }     
                return $temp;
        }  

        if($x<0) {
            $hasil = "minus ". trim(kekata($x));
        } else {
            $hasil = trim(kekata($x));
        }     
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }     
        return $hasil;
    }

   

    public static function access($role){
        $condition = false;
        foreach ($role as $key => $value) {                
            if(Auth::guard('admin')->user()->role == $value){
                $condition = true;
            }
        }
        return $condition;
    }

    public static function namaHari($val){
        if($val == 1){
            return "Senin";
        }else if($val == 2){
            return "Selasa";
        }else if($val == 3){
            return "Rabu";
        }else if($val == 4){
            return "Kamis";
        }else if($val == 5){
            return "Jumat";
        }else if($val == 6){
            return "Sabtu";
        }else if($val == 7){
            return "Minggu";
        }
    }

    public static function getHari($date){
        return self::namaHari(date('N',strtotime($date)));
    }

    public static function statusKehadiran($val){
        if($val == 1){
            return "Hadir";
        }else if($val == 2){
            return "Izin";
        }else if($val == 3){
            return "Sakit";
        }else if($val == 4){
            return "Tanpa Keterangan";
        }
    }

    public static function namaBulan($val){
        $nama_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
        return $nama_bulan[$val-1];
    }
}