<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Kategori;
use App\Kelas;
use App\Murid;
use App\Jadwal;
use Alert;
use Auth;
use DB;

class HomeController extends Controller
{
    public function index(){
        
        return view('home.index');
    }

    public function show($id){
        $properti = Properti::find($id);
        return view('home.show',compact('properti'));
    }

    public function store(Request $request){
        // dd($request->all());
        Pemesanan::create($request->all());
        Alert::make('success','Anda berhasil mengirim pesan, Marketing kami akan menghubungi anda');
        return back();
    }

    public function contact(){
        return view('home.contact');
    }

    public function akademik(){
        $id = Auth::guard('home')->user()->id;
        $data = Murid::find($id);
        $kelas = Kelas::where('murid_id',$id)->get();
        $jadwal = DB::table('jadwal')->select('kelas.id','jadwal.kode_hari','jadwal.jam','master_kelas.nama_kelas')->join('master_kelas','master_kelas.id','=','jadwal.master_kelas_id')
                          ->join('kelas','kelas.master_kelas_id','=','master_kelas.id')
                          ->groupBy('kelas.id','jadwal.kode_hari','jadwal.jam','master_kelas.nama_kelas')
                          ->where('murid_id',$id)
                          ->get();
        return view('home.akademik',compact('kelas','data','jadwal'));
    }
}
