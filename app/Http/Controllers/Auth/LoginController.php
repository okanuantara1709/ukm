<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('home.auth.login');
    }

    public function login(Request $request)
    {       

        $credentials = [
            'username' => $request->username,
            'password' => $request->password
        ];
        // dd($credentials);
        // dd(Auth::guard('admin')->attempt($credentials));
        if (Auth::guard('home')->attempt($credentials)) {
            if ($request->has('redirect')) {
                return redirect($request->redirect);
            }
            return redirect()->intended(route('home.index'));
        }else {
            Alert::make('danger','Pastikan username dan password benar.');
            return back();
        }
    }

    public function logout()
    {
        Auth::guard('home')->logout();
        return redirect(route('home.auth.login'));
    }
}
