<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Alert;
use App\Helpers\Render;
use App\User;
use App\Proposal;
use Auth;
use Mpdf;

class ProposalController extends Controller
{

    private $template = [
        'title' => 'Proposal',
        'route' => 'proposal',
        'menu' => 'proposal',        
        'icon' => 'fa fa-file'
    ]; 
        private function form(){
        
        return $form = [
            ['label' => 'Keterangan','name' => 'keterangan'],
            ['label' => 'File Proposal','name' => 'file', 'type' => 'file'],
            ['label' => 'Tanggal Proposal','name' => 'tanggal', 'type' => 'datepicker'],
            ['label' => 'user_id','name' => 'user_id','type' => 'hidden', 'value' => Auth::guard('admin')->user()->id],            
        ]; 
    }
    
    public function index(Request $request)
    {
        $template = (object) $this->template;
        $tahun = !empty($request->tahun) ? $request->tahun : date('Y');
       
        $data = Proposal::whereYear('tanggal','=',$tahun)->get();
       
        // resources/views/admin/proposal/index.blade.php
        return view('admin.proposal.index',compact('template','data','tahun'));
    }
    
    public function create()
    {
        $template = (object) $this->template;
        $form = $this->form();
        return view('admin.proposal.create',compact('template','form'));
    }

   
    public function store(Request $request)
    {        
        $data = $request->all();
        $file = $request->file('file');
        $data['file'] = $file->getClientOriginalName();
        $request->file('file')->move("image/", $file->getClientOriginalName());
        Proposal::create($data);

        Alert::make('success','Berhasil simpan data');
        return redirect(route('proposal.index'));
    }

    
    public function show($id)
    {
        $data = Proposal::find($id);
        // dd($data->nama);
        $template = (object) $this->template;
        return view('admin.proposal.show',compact('template','data'));
    }

    
    public function edit($id)
    {
        $data = Proposal::find($id);
        // dd($data->nama);
        $template = (object) $this->template;
        $form = $this->form();
        return view('admin.proposal.edit',compact('template','form','data'));
    }

    
    public function update(Request $request, $id)
    {
        
        $data = $request->all();
        $file = $request->file('file');
        $data['file'] = $file->getClientOriginalName();
        $request->file('file')->move("image/", $file->getClientOriginalName());
        Proposal::find($id)->update($data);
        Alert::make('success','Berhasil ubah data');
        return redirect(route('proposal.index'));
    }

    public function print($tahun){
        $data = Proposal::whereYear('tanggal','=',$tahun)->get();
        $page = view('admin.proposal.laporan')->with(['data' => $data]);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($page);
        $mpdf->Output();
    }
   
    public function destroy($id)
    {
        //
    }
}
