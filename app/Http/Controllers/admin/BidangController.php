<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Alert;
use App\Helpers\Render;
use App\User;
use App\Bidang;
use Auth;

class BidangController extends Controller
{

    private $template = [
        'title' => 'Bidang',
        'route' => 'bidang',
        'menu' => 'bidang',        
        'icon' => 'fa fa-info'
    ]; 
        private function form(){
        
        return $form = [
            ['label' => 'Nama Bidang','name' => 'nama_bidang'],
            
        ]; 
    }
    
    public function index()
    {
        $template = (object) $this->template;
        $data = Bidang::all();
        // dd($data);
        // resources/views/admin/bidang/index.blade.php
        return view('admin.bidang.index',compact('template','data'));
    }
    
    public function create()
    {
        $template = (object) $this->template;
        $form = $this->form();
        return view('admin.bidang.create',compact('template','form'));
    }

   
    public function store(Request $request)
    {        
        $data = $request->all();
        Bidang::create($data);

        Alert::make('success','Berhasil simpan data');
        return redirect(route('bidang.index'));
    }

    
    public function show($id)
    {
        $data = Bidang::find($id);
        // dd($data->nama);
        $template = (object) $this->template;
        return view('admin.bidang.show',compact('template','data'));
    }

    
    public function edit($id)
    {
        $data = Bidang::find($id);
        // dd($data->nama);
        $template = (object) $this->template;
        $form = $this->form();
        return view('admin.bidang.edit',compact('template','form','data'));
    }

    
    public function update(Request $request, $id)
    {
        
        $data = $request->all();
        Bidang::find($id)->update($data);
        Alert::make('success','Berhasil ubah data');
        return redirect(route('bidang.index'));
    }

   
    public function destroy($id)
    {
        //
    }
}
