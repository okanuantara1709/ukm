<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Alert;
use App\Helpers\Render;
use App\User;
use App\JadwalRadio;
use App\RadioUser;
use App\Kehadiran;
use Auth;

class JadwalRadioController extends Controller
{

    private $template = [
        'title' => 'Jadwal Radio',
        'route' => 'jadwal_radio',
        'menu' => 'jadwal_radio',        
        'icon' => 'fa fa-calendar'
    ]; 

    private function form(){
        $hari = [
            ['value' => 1, 'name' => 'Senin'],
            ['value' => 2, 'name' => 'Selasa'],
            ['value' => 3, 'name' => 'Rabu'],
            ['value' => 4, 'name' => 'Kamis'],
            ['value' => 5, 'name' => 'Jumat'],
            ['value' => 6, 'name' => 'Sabtu'],
            ['value' => 7, 'name' => 'Minggu'],
        ];
        
        return $form = [
            ['label' => 'Hari','name' => 'hari','type' => 'select', 'option' => $hari],
            ['label' => 'Jam', 'name' => 'jam'],
            ['label' => 'Sesi', 'name' => 'sesi']
        ]; 
    }
    
    public function index()
    {
        $template = (object) $this->template;
        $data = JadwalRadio::all();        
        // dd($data);
        // resources/views/admin/jadwal/index.blade.php
        return view('admin.jadwal_radio.index',compact('template','data'));
    }
    
    public function create()
    {
        $template = (object) $this->template;
        $form = $this->form();
        return view('admin.jadwal_radio.create',compact('template','form'));
    }

   
    public function store(Request $request)
    {        
        $data = $request->all();
        // dd($data);
        $jadwal = JadwalRadio::create($data);
       
        Alert::make('success','Berhasil simpan data');
        return redirect(route('jadwal_radio.index'));
    }

    
    public function show($id)
    {
        $data = JadwalRadio::find($id);
        $kehadiran  = RadioUser::select('kehadiran.tgl_hadir','radio_user.jadwal_radio_id','kehadiran.materi')
                        ->where('radio_user.jadwal_radio_id',$id)
                        ->join('kehadiran','kehadiran.radio_user_id','=','radio_user.id')
                        ->groupBy('kehadiran.tgl_hadir')
                        ->get();
                                                                        
        $template = (object) $this->template;
        return view('admin.jadwal_radio.show',compact('template','data','kehadiran'));
    }

    public function print($id)
    {
        $data = JadwalRadio::find($id);
        $kehadiran  = RadioUser::select('kehadiran.tgl_hadir','radio_user.jadwal_radio_id','kehadiran.materi')
                        ->where('radio_user.jadwal_radio_id',$id)
                        ->join('kehadiran','kehadiran.radio_user_id','=','radio_user.id')
                        ->groupBy('kehadiran.tgl_hadir')
                        ->get();

        $data_kehadiran= [];
        foreach ($kehadiran as $key => $value) {
            $array = $value;
            $array->kehadiran = Kehadiran::select('*','kehadiran.status as status_kehadiran')->join('radio_user','radio_user.id','=','kehadiran.radio_user_id')
                            ->join('user','user.id','radio_user.user_id')                            
                            ->where('radio_user.jadwal_radio_id',$value->jadwal_radio_id)
                            ->where('kehadiran.tgl_hadir',$value->tgl_hadir)
                            ->get();
            $data_kehadiran[] = $array;
        }

        // dd($data_kehadiran);
                        
        $page = view('admin.jadwal_radio.laporan')->with(['data' => $data_kehadiran]);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($page);
        $mpdf->Output();
       
    }

    
    public function edit($id)
    {
        $data = JadwalRadio::find($id);
        // dd($data->nama);
        $template = (object) $this->template;
        $form = $this->form();
        return view('admin.jadwal_radio.edit',compact('template','form','data'));
    }

    
    public function update(Request $request, $id)
    {        
        $data = $request->all();
        JadwalRadio::find($id)->update($data);
        Alert::make('success','Berhasil ubah data');
        return redirect(route('jadwal_radio.index'));
    }

    private function formUser($id){   
        $user_available = RadioUser::where('jadwal_radio_id',$id)->get()->pluck('user_id');
        // dd($user_available);
        $user = User::select('id as value','nama as name')->whereNotIn('id',$user_available)->get();   
        return $form = [
            ['label' => 'jadwal_radio_id', 'name' => 'jadwal_radio_id','type' => 'hidden','value' => $id],
            ['label' => 'Nama Anggota', 'name' => 'user_id','type' => 'select','option' => $user],
        ]; 
    }

    public function createUser(Request $request, $id){
        $template = (object) $this->template;        
        $form = $this->formUser($id);
        return view('admin.jadwal_radio.createUser',compact('template','form','id'));
    }

    public function storeUser(Request $request, $id){
        $data = $request->all();        
        $jadwal = RadioUser::create($data);
       
        Alert::make('success','Berhasil simpan data');
        return redirect(route('jadwal_radio.show',$id));
    }

    public function deleteUser($id)
    {
        $radio_user = RadioUser::find($id)->delete();
        Alert::make('success','Berhasil hapus data');
        return back();
    }

    private function formKehadiran($radio_user_id){
        return $form = [
            ['label' => 'Tanggal Kehadiran','name' => 'tgl_hadir','type' => 'datepicker'],
            ['label' => 'Materi','name' => 'materi','type' => 'text'],
            ['label' => 'radio user','name' => 'radio_user_id','type' => 'hidden', 'value' => $radio_user_id],
        ]; 
    }

    public function createKehadiran($id){
        $template = (object) $this->template;        
        $form = $this->formKehadiran($id);
        $user = User::join('radio_user','radio_user.user_id','=','user.id')
                    ->where('jadwal_radio_id',$id)
                    ->get();       
        return view('admin.jadwal_radio.createKehadiran',compact('template','form','id','user'));
    }

    public function editKehadiran(Request $request,$id){
        $template = (object) $this->template;
        $tgl_hadir = $request->tgl_hadir;
        $form = $this->formKehadiran($id);
        $user = Kehadiran::select('kehadiran.*')->join('radio_user','radio_user.id','=','kehadiran.radio_user_id')                           
                            ->where('radio_user.jadwal_radio_id',$id)
                            ->where('kehadiran.tgl_hadir',$tgl_hadir)
                            ->get(); 

        $data = Kehadiran::join('radio_user','radio_user.id','=','kehadiran.radio_user_id')                            
                            ->where('radio_user.jadwal_radio_id',$id)
                            ->where('kehadiran.tgl_hadir',$tgl_hadir)
                            ->first();   
                            // dd($user);

        return view('admin.jadwal_radio.editKehadiran',compact('template','form','id','user','data'));
    }

    public function storeKehadiran(Request $request, $id){
        $data = $request->user;    
        foreach ($data as $key => $value) {
            // $data[$key]['status'] = $value['status'];
            $data[$key]['tgl_hadir'] = $request->tgl_hadir;
            $data[$key]['materi'] = $request->materi;
        }
        // dd($data);
        Kehadiran::insert($data);
       
        Alert::make('success','Berhasil simpan data');
        return redirect(route('jadwal_radio.show',$id));
    }

    public function updateKehadiran(Request $request, $id){
        $data = $request->user;    
        // dd($request->all());
        foreach ($data as $key => $value) {
            $value['status'] = $value['status'];
            $value['tgl_hadir'] = $request->tgl_hadir;
            $value['materi'] = $request->materi;
            // dd($key);
            Kehadiran::where('id',$key)->update($value);
        }
        // dd($data);
       
       
        Alert::make('success','Berhasil ubah data');
        return redirect(route('jadwal_radio.show',$id));
    }

    public function showKehadiran(Request $request,$id){
        $template = (object) $this->template;
        $data = JadwalRadio::find($id);
        $kehadiran  = RadioUser::select('kehadiran.tgl_hadir','radio_user.jadwal_radio_id','kehadiran.materi')
                        ->where('radio_user.jadwal_radio_id',$id)
                        ->where('kehadiran.tgl_hadir',$request->tgl_hadir)
                        ->join('kehadiran','kehadiran.radio_user_id','=','radio_user.id')
                        ->groupBy('kehadiran.tgl_hadir')
                        ->get();

        $data_kehadiran= [];
        foreach ($kehadiran as $key => $value) {
            $array = $value;
            $array->kehadiran = Kehadiran::select('*','kehadiran.status as status_kehadiran')->join('radio_user','radio_user.id','=','kehadiran.radio_user_id')
                            ->join('user','user.id','radio_user.user_id')                            
                            ->where('radio_user.jadwal_radio_id',$value->jadwal_radio_id)
                            ->where('kehadiran.tgl_hadir',$value->tgl_hadir)
                            ->get();
            $data_kehadiran[] = $array;
        }

        // dd($data_kehadiran);
        return view('admin.jadwal_radio.showKehadiran',compact('template','data_kehadiran','kehadiran','data'));
    }

}
