<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Alert;
use App\Helpers\Render;
use App\User;
use App\Jadwal;
use App\JadwalUser;
use Auth;
use DB;

class JadwalController extends Controller
{

    private $template = [
        'title' => 'Jadwal',
        'route' => 'jadwal',
        'menu' => 'jadwal',        
        'icon' => 'fa fa-calendar'
    ]; 

    private function form($id = false){
        
        $user = User::select('id as value',DB::raw("CONCAT(nama,' | ',jenis_kelamin) as name"))->where('status',1)->get();
        
    

        // dd($user);
        
        
        return $form = [
            ['label' => 'User Id','name' => 'user_id','type' => 'hidden','value' => Auth::guard('admin')->id()],
            ['label' => 'Jadwal','name' => 'jadwal','type' => 'datepicker'],
            ['label' => 'Surat','name' => 'surat','type' => 'file','required' => false],
            ['label' => 'Lokasi','name' => 'lokasi','type' => 'text'],
            ['label' => 'Keterangan','name' => 'keterangan','type' => 'ckeditor'],
            ['label' => 'Untuk Anggota','name' => 'list_user','type' => 'select','option' => $user,'attr' => 'multiple','class' => 'select2'],
        ]; 
    }
    
    public function index(Request $request)
    {
        $template = (object) $this->template;
        $bulan = empty($request->bulan) ? date('m') : $request->bulan;
        $tahun = empty($request->tahun) ? date('Y') : $request->tahun; 
        $data = Jadwal::whereYear('jadwal',$tahun)->whereMonth('jadwal',$bulan)->orderBy('jadwal','asc')->get();
        
        $data_grafik = [];
        for($i=1;$i<=12;$i++){
            $total = Jadwal::whereYear('jadwal',$tahun)->whereMonth('jadwal',$i)->orderBy('jadwal','asc')->get()->count();
            // dd($total);
            array_push($data_grafik,$total);
        }
        // dd($data_grafik);
        $data_grafik = json_encode($data_grafik);
        // resources/views/admin/jadwal/index.blade.php
        return view('admin.jadwal.index',compact('template','data','tahun','bulan','data_grafik'));
    }
    
    public function create()
    {
        $template = (object) $this->template;
        $form = $this->form();
        return view('admin.jadwal.create',compact('template','form'));
    }

   
    public function store(Request $request)
    {        
        $data = $request->all();
        $file = $request->file('surat');
        $data['surat'] = $file->getClientOriginalName();
        $request->file('surat')->move("image/", $file->getClientOriginalName());
        // dd($data);
        $jadwal = Jadwal::create($data);
        $jadwal_id = $jadwal->id;
        $list_user = $request->list_user;
        // dd($list_user);
        foreach ($request->list_user as $key => $value) {
            JadwalUser::create([
                'jadwal_id' => $jadwal_id,
                'user_id' => $value
            ]);
        }
        Alert::make('success','Berhasil simpan data');
        return redirect(route('jadwal.index'));
    }

    
    public function show($id)
    {
        $data = Jadwal::find($id);
        // dd($data->nama);
        $template = (object) $this->template;
        return view('admin.jadwal.show',compact('template','data'));
    }

    
    public function edit($id)
    {
        $data = Jadwal::find($id);
        $data->list_user = JadwalUser::where('jadwal_id',$id)->pluck('user_id');
        // dd($data);
        $template = (object) $this->template;
        $form = $this->form($id);
        return view('admin.jadwal.edit',compact('template','form','data'));
    }

    
    public function update(Request $request, $id)
    {
        
        $data = $request->all();
        Jadwal::find($id)->update($data);
        JadwalUser::where('jadwal_id',$id)->delete();
        foreach ($request->list_user as $key => $value) {
            JadwalUser::create([
                'jadwal_id' => $id,
                'user_id' => $value
            ]);
        }
        Alert::make('success','Berhasil ubah data');
        return redirect(route('jadwal.index'));
    }

   
    public function print(Request $request){
        $bulan = empty($request->bulan) ? date('Y') : $request->bulan;
        $tahun = empty($request->tahun) ? date('Y') : $request->tahun; 
        $data = Jadwal::whereYear('jadwal',$tahun)->whereMonth('jadwal',$bulan)->orderBy('jadwal','asc')->get();

        $page = view('admin.jadwal.laporan')->with(['data' => $data,'tahun' => $tahun,'bulan' => $bulan]);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($page);
        $mpdf->Output();
    }
   
}
