<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RadioUser extends Model
{
    protected $table = 'radio_user';
    protected $fillable = [
        'jadwal_radio_id','user_id'
    ];   

    public function user(){
        return $this->belongsTo('App\User');
    }
}
