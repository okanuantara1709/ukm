<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kehadiran extends Model
{
    protected $table = 'kehadiran';
    protected $filled = ['status','tgl_hadir','materi','jadwal_radio_id'];

    public function radio_user(){
        return $this->belongsTo('App\RadioUser');
    }
}
