<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal_mc';
    protected $fillable = [
        'jadwal', 'keterangan','user_id','surat','lokasi'
    ];

    public function jadwal_user(){
        return $this->hasMany('App\JadwalUser');
    }
    
   
}
