<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalUser extends Model
{
    protected $table = 'jadwal_user';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
