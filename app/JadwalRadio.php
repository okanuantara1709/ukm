<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalRadio extends Model
{
    protected $table = 'jadwal_radio';
    protected $fillable = [
        'hari', 'jam','sesi'
    ];

    public function anggota(){
        return $this->hasMany('App\RadioUser');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
    
   
}
