@extends('admin.layouts.app')
@push('css')

@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$template->title}}                
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">{{$template->title}}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="{{$template->icon}}"></i> List {{$template->title}}</h3>
                            @if(AppHelper::access(['Kordinator']) && Auth::guard('admin')->user()->bidang_id == 3)
                            <a href="{{route("$template->route".'.create')}}" class="btn btn-primary pull-right">
                                <i class="fa fa-pencil"></i> Tambah {{$template->title}}
                            </a>
                            @endif

                            
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <form action="" method="get">
                                    <div class="col-md-2">
                                        <select name="tahun" id="" class="form-control">
                                            @for($i = 2017;$i < 2030;$i++)
                                                <option value="{{$i}}" {{AppHelper::selected($tahun,$i)}}>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-success">Cari</button>
                                    </div>
                                </form>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>                                        
                                        <th>Keterangan</th>    
                                        <th>File</th>
                                        @if(AppHelper::access(['admin','Kordinator','Anggota']) && Auth::guard('admin')->user()->bidang_id == 3)                                
                                        <th style="width:200px">Aksi</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key => $row)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$row->keterangan}}</td>  
                                            <td><a href="{{asset("image/$row->file")}}" class="btn btn-success" target="__blank">Lihat File</a></td>                                         
                                            <td>
                                                 @if(AppHelper::access(['admin','Kordinator','Anggota']) && Auth::guard('admin')->user()->bidang_id == 3)
                                                    <a href="{{route("$template->route".'.edit',[$row->id])}}" class="btn btn-success btn-sm">Ubah</a>                                                
                                                @endif
                                                {{-- <a href="{{route("$template->route".'.show',[$row->id])}}" class="btn btn-info btn-sm">Lihat</a> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if(AppHelper::access(['Admin']) ||AppHelper::access(['admin','Kordinator','Anggota']) && Auth::guard('admin')->user()->bidang_id == 3)
                            <a href="{{route("$template->route".'.print',[$tahun])}}" target="__blank" class="btn btn-default pull-right">
                                <i class="fa fa-print"></i> Print
                            </a>
                            @endif
                          
                        </div>
                    </div>
                </div>
           </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('js')
    <!-- page script -->
    <script>
    $(function () {
        $('#datatables').DataTable()
        $('#full-datatables').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
        })
    })
    </script>
@endpush