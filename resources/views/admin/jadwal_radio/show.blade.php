@extends('admin.layouts.app')
@push('css')

@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$template->title}}                
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">{{$template->title}}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="{{$template->icon}}"></i> Detail {{$template->title}}</h3>
                            @if(AppHelper::access(['admin','Kordinator','Anggota']) && Auth::guard('admin')->user()->bidang_id == 1)                            
                            <a href="{{route("$template->route".'.createUser',$data->id)}}" class="btn btn-primary pull-right">
                                <i class="fa fa-pencil"></i> Tambah Anggota Penyiar
                            </a>
                            @endif
                        </div>
                        <div class="box-body">  
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width:200px"></th>
                                        <th style="width:20px"></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tbody>                                                                                       
                                        <tr>
                                            <td>Jadwal</td>
                                            <td>:</td>
                                            <td>{{AppHelper::namaHari($data->hari)." ".$data->jam}}</td>
                                        </tr>
                                        <tr>
                                            <td>Sesi</td>
                                            <td>:</td>
                                            <td>{{$data->sesi}}</td>
                                        </tr>
                                        <tr>
                                            <td>Anggota</td>
                                            <td>:</td>
                                            <td>
                                                <ol style="padding:15px;padding-top:0px">
                                                    @foreach($data->anggota as $key => $value)
                                                        <li>{{$value->user->nama}} 
                                                        @if(AppHelper::access(['Kordinator']) && Auth::guard('admin')->user()->bidang_id == 1)
                                                        <a href="{{route("$template->route".".deleteUser",$value->id)}}" onclick="return  confirm('Yakin hapus user ini ?')"><i class="fa fa-trash text-danger"></i></a></li>
                                                        @endif
                                                    @endforeach
                                                 
                                                </ol>
                                            </td>
                                        </tr>                                                                      
                                    </tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">                                
                            <a href="{{ route("$template->route.index") }}" class="btn btn-default">Kembali</a>
                        </div>
                        
                    </div>

                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="{{$template->icon}}"></i> Kehadiran</h3>   
                            @if(AppHelper::access(['admin','Kordinator','Anggota']) && Auth::guard('admin')->user()->bidang_id == 1)                         
                            <a href="{{route("$template->route".'.createKehadiran',$data->id)}}" class="btn btn-primary pull-right">
                                <i class="fa fa-pencil"></i> Tambah Kehadiran
                            </a>
                            @endif
                        </div>
                        <div class="box-body">  
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Hadir</th>
                                        <th>Materi</th>
                                        <th>Aksi</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <tbody>     
                                        @foreach($kehadiran as $key => $value)                                                                                  
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$value->tgl_hadir}}</td>  
                                                <td>{{$value->materi}}</td>
                                                
                                                <td>
                                                @if(AppHelper::access(['admin','Kordinator','Anggota']) && Auth::guard('admin')->user()->bidang_id == 1)
                                                    <a href="{{route($template->route.".editKehadiran",[$value->jadwal_radio_id, 'tgl_hadir' => $value->tgl_hadir])}}" class="btn btn-success">Ubah</a>
                                                    @endif
                                                    <a href="{{route($template->route.".showKehadiran",[$value->jadwal_radio_id, 'tgl_hadir' => $value->tgl_hadir])}}" class="btn btn-info">Lihat</a>
                                                
                                                </td>  
                                                                                        
                                                
                                            </tr>  
                                               
                                        @endforeach                                                                                                 
                                    </tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">                                
                            <a href="{{ route("$template->route.index") }}" class="btn btn-default">Kembali</a>
                            @if(AppHelper::access(['Admin']) || AppHelper::access(['admin','Kordinator','Anggota']) && Auth::guard('admin')->user()->bidang_id == 1)
                                <a href="{{ route('jadwal_radio.print',[$data->id]) }}" class="btn btn-primary"><div class="fa fa-print"></div> Print</a>
                            @endif
                        </div>
                       
                        
                    </div>
                </div>
           </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('js')
    <!-- page script -->
    <script>
    $(function () {
        $('#datatables').DataTable()
        $('#full-datatables').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
        })
    })
    </script>
@endpush