@extends('admin.layouts.app')
@push('css')

@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$template->title}} MC               
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">{{$template->title}}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        
           <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="{{$template->icon}}"></i> List {{$template->title}} MC</h3>
                             @if(AppHelper::access(['Kordinator']) && Auth::guard('admin')->user()->bidang_id == 2)
                            <a href="{{route("$template->route".'.create')}}" class="btn btn-primary pull-right">
                                <i class="fa fa-pencil"></i> Tambah {{$template->title}}
                            </a>
                            @endif
                        </div>
                        <div class="box-body">
                        <div class="row">
                            <form action="" method="get">
                                <div class="col-md-2">
                                    <select name="bulan" id="" class="form-control">
                                        @for($i = 1;$i <= 12;$i++)
                                            <option value="{{$i}}" {{AppHelper::selected($bulan,$i)}}>{{AppHelper::namaBulan($i)}}</option>
                                        @endfor
                                    </select>                                    
                                </div>
                                <div class="col-md-2">
                                    <select name="tahun" id="" class="form-control">
                                        @for($i = 2017;$i < 2030;$i++)
                                            <option value="{{$i}}" {{AppHelper::selected($tahun,$i)}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-success">Cari</button>
                                </div>
                            </form>
                        </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>                                        
                                        <th>Jadwal</th> 
                                        <th>Surat</th>                                 
                                        <th>Aksi</th>                      
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key => $row)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$row->jadwal}}</td>                                        
                                            <td><a target="__blank" class="btn btn-success btn-xs" href="{{asset("image/$row->surat")}}"><i class="fa fa-download"></i> Surat</a></td>
                                            <td>
                                                @if(AppHelper::access(['admin','Kordinator','Anggota']) && Auth::guard('admin')->user()->bidang_id == 2)
                                                    <a href="{{route("$template->route".'.edit',[$row->id])}}" class="btn btn-success btn-sm">Ubah</a>                                                
                                                @endif
                                                <a href="{{route("$template->route".'.show',[$row->id])}}" class="btn btn-info btn-sm">Lihat</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if(AppHelper::access(['Admin']) || (AppHelper::access(['Admin','Kordinator']) && Auth::guard('admin')->user()->bidang_id == 2))
                            <a href="{{route("$template->route".'.print',['tahun' => $tahun,'bulan' => $bulan])}}" target="__blank" class="btn btn-default pull-right">
                                <i class="fa fa-print"></i> Print
                            </a>
                            @endif
                            
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title pull-left">Grafik MC Perbulan Tahun {{$tahun}}</h1>                           
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="areaChart" style="height:250px"></canvas>
                            </div>
                        </div>
                    </div>
                </div> 
           </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('js')
    <!-- page script -->
    <script src="{{asset('admin-lte')}}/bower_components/chart.js/Chart.js"></script>
    <script>
    $(function () {
        $('#datatables').DataTable()
        $('#full-datatables').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
        })
    })

    $('#datatables').DataTable()
            $('#full-datatables').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })

            var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
            // This will get the first returned node in the jQuery collection.
            var areaChart       = new Chart(areaChartCanvas)
            var data = @php echo $data_grafik @endphp 

            

               

            var areaChartData = {
            labels  : ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','Sepetember','Oktober','November','Desember'],
            datasets: [
                {
                    label               : 'Jumlah Bayi',
                    fillColor           : 'rgba(210, 214, 222, 1)',
                    strokeColor         : 'rgba(210, 214, 222, 1)',
                    pointColor          : 'rgba(210, 214, 222, 1)',
                    pointStrokeColor    : '#c1c7d1',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data                : data
                }
                
            ]
            }

            var areaChartOptions = {
            //Boolean - If we should show the scale at all
            showScale               : true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines      : false,
            //String - Colour of the grid lines
            scaleGridLineColor      : 'rgba(0,0,0,.05)',
            //Number - Width of the grid lines
            scaleGridLineWidth      : 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines  : true,
            //Boolean - Whether the line is curved between points
            bezierCurve             : true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension      : 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot                : false,
            //Number - Radius of each point dot in pixels
            pointDotRadius          : 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth     : 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke           : true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth      : 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill             : false,
            //String - A legend template
            legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio     : true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive              : true,
            multiTooltipTemplate    : "<%= datasetLabel %>: <%= value %>",
            legend                  : {
                                        display: true,
                                        labels: {
                                            fontColor: 'rgb(255, 99, 132)'
                                        }
                                    }
            
            }

            //Create the line chart
            areaChart.Line(areaChartData, areaChartOptions)


    </script>
@endpush