@extends('home.layouts.app')
@push('css')
   
@endpush
@section('content')
    <section>
        <div class="col-sm-12 header">
            <div class="row">
                <img src="{{asset('image/bg-header.jpg')}}" alt="gambar tari" width="100%">               
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Dende Anjani</h1>
                    <p class="text-center">
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi consectetur inventore ad! Libero vero qui architecto aspernatur reprehenderit, repellendus quo, autem veritatis non dignissimos optio esse dolorum illum, commodi accusamus?
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Kelas Di Sangar Tari Dende Anjani</h1>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{asset('image/bg-header.jpg')}}" alt="gambar tari" width="100%">
                            <h4>Tari Tradisional</h4>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem ipsam nihil impedit nesciunt cumque. Rerum voluptates accusantium omnis! Ea iusto excepturi nostrum voluptas distinctio, ipsam fuga totam id reprehenderit veniam!
                            </p>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('image/bg-header.jpg')}}" alt="gambar tari" width="100%">
                            <h4>Tari Tradisional</h4>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem ipsam nihil impedit nesciunt cumque. Rerum voluptates accusantium omnis! Ea iusto excepturi nostrum voluptas distinctio, ipsam fuga totam id reprehenderit veniam!
                            </p>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('image/bg-header.jpg')}}" alt="gambar tari" width="100%">
                            <h4>Tari Tradisional</h4>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem ipsam nihil impedit nesciunt cumque. Rerum voluptates accusantium omnis! Ea iusto excepturi nostrum voluptas distinctio, ipsam fuga totam id reprehenderit veniam!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
@endsection
@push('js') 
    
    <script>
        var filterizd = $('.filtr-container').filterizr({
           //options object
        });

        $(".filtr-button").click(function(){
            $(".filtr-button").removeClass('active');
            $(this).addClass('active');
        })
    </script>
@endpush