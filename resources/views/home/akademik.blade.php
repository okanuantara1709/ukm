@extends('home.layouts.app')
@push('css')
     <link rel="stylesheet" href="{{asset("css/flexslider.css")}}">
@endpush
@section('content')

    
    <section>
        <div class="col-md-12" style="margin-top:100px">
            {!!Alert::showAlert()!!}
            <div class="container" style="min-height:500px">
                <div class="col-md-12">
                    <div class="box-body">  
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width:200px"></th>
                                    <th style="width:20px"></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tbody>                                                                                       
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{$data->nama}}</td>
                                    </tr>
                                    <tr>
                                        <td>Username</td>
                                        <td>:</td>
                                        <td>{{$data->username}}</td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td>{{$data->telepon}}</td>
                                    </tr>                                        
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        <td>{{$data->jenis_kelamin}}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td>{{$data->status == 1 ? 'Aktif' : 'Tidak Aktif'}}</td>
                                    </tr>
                                </tbody>
                            </tbody>
                        </table>  

                        <h3>Kelas Yang Di Ikuti</h3>
                        <table class="table" id="datatables">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>                                        
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($kelas as $key => $row)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$row->master_kelas->nama_kelas}}</td>                                            
                                        <td>                                           
                                            <a href="{{route("kehadiran.index",[$row->id])}}" class="btn btn-primary btn-sm">Kehadiran</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table> 
                        
                        <h3>Jadwal Yang Di Ikuti</h3>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kelas</th>
                                    <th>Jadwal</th>
                                    <th>Jam</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tbody> 
                                    @foreach($jadwal as $key => $value)                                                                                      
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$value->nama_kelas}}</td>
                                        <td>{{AppHelper::namaHari($value->kode_hari)}}</td>
                                        <td>{{$value->jam}}</td>  
                                        <td>
                                            
                                        </td>                                          
                                    </tr> 
                                    @endforeach
                                                                            
                                </tbody>
                            </tbody>
                        </table>
                </div>                
            </div>
        </div>
    </section>
@endsection
@push('js')  

@endpush