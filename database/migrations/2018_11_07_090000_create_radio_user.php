<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRadioUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('radio_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('jadwal_radio_id')->unsigned();
            $table->timestamps();            

             $table->foreign('user_id')
                    ->references('id')->on('user')
                    ->onDelete('cascade')
                    ->onUpdate('cascade'); 
                    
            $table->foreign('jadwal_radio_id')
                    ->references('id')->on('jadwal_radio')
                    ->onDelete('cascade')
                    ->onUpdate('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('radio_user');
    }
}
