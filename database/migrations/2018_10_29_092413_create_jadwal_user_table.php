<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('jadwal_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')
                    ->references('id')->on('user')
                    ->onDelete('cascade')
                    ->onUpdate('cascade'); 

            $table->foreign('jadwal_id')
                    ->references('id')->on('jadwal')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_user');
    }
}
